package com.andressalgadoc.materialdesigned.utils;

public class Constants {

    // paso de parametros en la MainActivity y Scroll/Static Activity
    public static final String ARG_NAME = "name";

    // type de nuestro objeto component
    public static final int SCROLL = 0;
    public static final int STATIC = 1;
}
