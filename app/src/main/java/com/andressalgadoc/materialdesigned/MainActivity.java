package com.andressalgadoc.materialdesigned;

import android.content.Intent;
import android.os.Bundle;

import com.andressalgadoc.materialdesigned.adapters.ComponentAdapter;
import com.andressalgadoc.materialdesigned.framents.BottomNavigationBarFragment;
import com.andressalgadoc.materialdesigned.framents.ButtonFragment;
import com.andressalgadoc.materialdesigned.framents.CheckBoxFragment;
import com.andressalgadoc.materialdesigned.framents.FloatingActionButtonFragment;
import com.andressalgadoc.materialdesigned.framents.SnackBarFragment;
import com.andressalgadoc.materialdesigned.framents.TextFieldFragment;
import com.andressalgadoc.materialdesigned.utils.Component;
import com.andressalgadoc.materialdesigned.utils.Constants;
import com.andressalgadoc.materialdesigned.utils.OnClickListener;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements OnClickListener {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private ComponentAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        configAdapter();
        configAdapterRecyclerView();

    }

    private void configAdapter() {
        mAdapter = new ComponentAdapter(new ArrayList<>(), this);
        mAdapter.add(ButtonFragment.getmInstance());
        mAdapter.add(BottomNavigationBarFragment.getmInstance());
        mAdapter.add(SnackBarFragment.getmInstance());
        mAdapter.add(TextFieldFragment.getmInstance());
        mAdapter.add(FloatingActionButtonFragment.getmInstance());
        mAdapter.add(CheckBoxFragment.getmInstance());
    }

    private void configAdapterRecyclerView() {
        recyclerView.setAdapter(mAdapter);
    }

    /*
    * OnClickListener
    * */
    @Override
    public void onClick(Component component) {
        Intent intent;
        if(component.getType() == Constants.SCROLL) {
            intent = new Intent(this,ScrollActivity.class);
        } else {
            intent = new Intent(this, StaticActivity.class);
        }
        intent.putExtra(Constants.ARG_NAME, component.getName());
        startActivity(intent);

    }
}